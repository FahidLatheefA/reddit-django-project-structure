# Reddit Django Project Structure

Discusses the user stories and table structure of Reddit project

# User Stories

## Basic Options
1. As a content writer, I want to **share** my content with other people

        Feature: New Post Creation


2. As a student, I want some **help** and **feedback** on my work

        Features: New Post Creation, Posts comment section


3. As a football fan, I want to **share** the key highlights of the match

        Feature: Media (Video/Image) Post Creation


4. As a cricket fan, I want to **watch** the key highlights of the match

        Feature: Media (Video/Image) download/watch online option


5. As an avid reader, I want to **read** about technology and politics

        Feature: Topic-wise forums(subreddit)


6. I would like to have a **discussion**/Conversation between other people around the world

        Feature: Discussion/comment section in Posts


7. I would like to **give feedback** to each posts/comments

        Feature: Downvote/Upvote option

8. I want to **create account** and save all my comments/posts

        Feature: Sign in Sign Up as a new user


## Miscellaneous Options
1. I would like to **format** my content in Markdown

        Feature: Markdown support


2. I would like to be a **moderator** for Python forum who can **delete inappropriate** content

        Feature: Mods, Mods with deleting, banning of user, Other mods features like pinning post


3. I would like to **subscribe** forums and **save posts**

        Feature: Subscribe option (subreddit), save posts option


4. I would like to earn **respect** over time for my efforts in the website

        Feature: Karma (Cumulative points)


5. I would like to **reward** other user for his post/comment/effort

        Feature: Reward gold/silver/platinum award


6. I would like **interact** with people on a personal level

        Feature: DM option



# Database structure

4 tables

1. **Users table**
2. **Subreddit list table** (foreign key: `user_id`)
3. **Posts table** (foreign keys: `sub_id`, `user_id`)
4. **Comments table** (foreign keys: `sub_id`, `user_id`, `post_id`)


### Users table
| user_id | user_name | user_email| password| current karma| timestamp|
|--|--|--|--|--|--|
|1|  ajay| ajay123@gmail.com| encrypted_pass|34 |1632378124000
|2  | anil | anil345@yahoo.co.in| encrypted_pass|199 |1632378124000|
|3  | .. |..| ..|.. |..|

### Subreddit list table (For all created subreddits)

| sub_id | sub_name |creator_id | minimum_karma_for_post|minimum_karma_for_comment| sub_link|timestamp
|--|--|--|--|--|--|--|
|1  |  technology| 2| 20|0|/technology|1632378124000
|2  | news | 2| 100|50|/news|1632378124000
|3  | .. |..| ..|..|..

### Posts table (For all created posts in all subreddits)

| post_id | sub_id |post_creator_id | comments_allowed(bool)|pinned(bool)| content header | media_post (bool) | content | media_link (if media_post=1)|timestamp|
|--|--|--|--|--|--|--|--|--|--|
|1  |  2| 2| True|False|Welcome|False|Hello, how are you||1632378124000
|2  | 3 | 1| False|False| Testing posting abilty|True| | https://imgur.com/gallery/cBHZ1vI|1632378124000
|3  | .. |..| ..|..|..|..


### Comments table (For all posts in all subreddits)

comment_id|sub_id| post_id | commenter_id |comment_content | timestamp| reply_to_comment_id (NULL if it is a level 1 reply)
|--|--|--|--|--|--|--|
|1|4|1  | 4| Testing|1632378124000 | |
|2|2|1  | 3 | Hi|1632378125000 | 1 |
|3|2|3  | 2 | https://imgur.com/gallery/cBHZ1vI| 1632378124000| |
|.. | .. |..|..|..|..|..|
